require([
    "TweenLite",
    "CSSPlugin",
    "EasePack",
    "TimelineLite"
],function(TweenLite, CSSPlugin, EasePack, TimelineLite) {
    
    var jplayer = $(".Bloc-medias"),
        jslider = $(".Medias-mobile", jplayer),
        jitems = $(".Medias-visuel", jplayer)
    ;
    
    $(window).resize(function() {
        current = 0;
        w = jitems.width() + 30; // largeur d'un item
        length = jitems.length; // nb total d'item
        TweenLite.set(jslider, {x: 0, ease:Power3.easeInOut});
    });
	
	$(function(){
		
        if($('.Medias').length > 0){
		
		    var _viewer = new MediaViewer(),
                jwin = $(window)
            ;
            jwin.resize(function(){_viewer.resize();})

            var current = 0,// quel est le premier item affiché
                sel = 4,// combien d'item visible à la fois
                w = jitems.width() + 30, // largeur d'un item
                length = jitems.length// nb total d'item
            ;

            var animate = function(){
                TweenLite.to(jslider, .9, {x:- current * w, ease:Power3.easeInOut});
            };
            $(".next", jplayer).click(function(){
                current = ++current % (length - sel + 1);
                animate();
            });
            $(".prev", jplayer).click(function(){
                current = (current == 0 ? (length - sel + 1) : current) - 1;
                animate();
            });


            var tab_url = [];
            jitems.each(function(n){
                var jthis = $(this);
                tab_url.push(jthis.attr("data-big"));
            });

            jitems.click(function(){
                var n = $(this).index();
                _viewer.open(tab_url, n);

            })
        
        }
        	
  });
	
	
	function MediaViewer(){};
	MediaViewer.prototype.current = 0;
	MediaViewer.prototype._current = null;
	MediaViewer.prototype.total = null;
	MediaViewer.prototype.element = null;
	MediaViewer.prototype.container = null;
	MediaViewer.prototype.tab_url = null;
	MediaViewer.prototype.tlShow = null;
	MediaViewer.prototype.close = function(){
		$("body").removeClass("noScroll");
//		this.element.remove();
		this.tlShow.timeScale(1.5).reverse();
		
	};
	MediaViewer.prototype._closeComplete = function(){
		this.element.remove();
	};
	MediaViewer.prototype.open = function(tab, n){
		
		this.tab_url = tab;
		this.current = n;
		this.total = tab.length;
		
		var jthis = this.element = this._createElement(); 
		$("body").addClass("noScroll").append(jthis);
		
		this.animate();
		
	};
	
	
	MediaViewer.prototype.next = function(){
		this.current = ++this.current % (this.total);
		this.animate();
	};
	MediaViewer.prototype.prev = function(){
		this.current = (this.current == 0 ? this.total : this.current) - 1;
		this.animate();
	};
	
	MediaViewer.prototype.animate = function(){
		var jcontainer = this.container,
			jold = $("img", jcontainer),
			refThis = this
		;
		if(jold.length > 0){
			TweenLite.to(jold, .3, {autoAlpha:0, onComplete:function(){refThis._showCurrent();}});
		}
		
		this._loadCurrent();
	};
	MediaViewer.prototype._showCurrent = function(){
		var img = this._current;
		if(!img || !img.complete){
			return;
		}
		var jcontainer = this.container,
			jold = $("img", jcontainer)
		;
		jold.remove();
		jcontainer.append(img);
		this.resize();
		
		TweenLite.to(img, .5, {opacity:1});
		
	};
	MediaViewer.prototype.resize = function(){
		var refThis = this,
			jcontainer = this.container,
			jimg = $("img", jcontainer),
			stage = {width:jcontainer.width(), height:jcontainer.height()}
		;
		for(var i = 0, l = jimg.length; i<l; i++){
			this._resize(jimg.eq(i), stage);
		}
	};
		
	MediaViewer.prototype._resize = function(jimg, stage){
		var w0 = jimg.attr("data-width"),
			h0 = jimg.attr("data-height"),
			w1 = stage.width,
			h1 = stage.height,
			k = Math.min(1, w1 / w0, h1 / h0),
			w = w0 * k, h = h0 * k,
			x = (w1 - w) * .5, y = (h1 - h) * .5
		;
		TweenLite.set(jimg, {x:x, y:y, width:w, height:h});
	};
	MediaViewer.prototype._loadComplete = function(){
		
		var refThis = this,
			img = this._current
		;
		
		if (!img || !img.complete){return;}
		if(!img.width){
			setTimeout(function(){refThis._loadComplete();},10);
		}
		
		var jcontainer = this.container,
			jimg = $(img)
		;
		jimg.attr("data-width", img.width);
		jimg.attr("data-height", img.height);
		jimg.css({display:"block", position:"absolute"});
		jcontainer.append(jimg);
		this._showCurrent();
	};
	MediaViewer.prototype._loadCurrent = function(){
		var refThis = this,
			img = new Image()
		;
		img.onload = function(){
			refThis._loadComplete();
		};
		img.src = this.tab_url[this.current];
		this._current = img;
	};
	MediaViewer.prototype._createElement = function(){
		var refThis = this,
			jthis = $('<div class="MediaViewer"></div>'),
			btnClose = $('<div class="MediaViewer-btn close">&#10005;</div>'),
			btnPrev = $('<div class="MediaViewer-btn prev">&#8592;</div>'),
			btnNext = $('<div class="MediaViewer-btn next">&#8594;</div>')
		;
		this.container = $('<div class="MediaViewer-container"><div class="Loading-loader shown red"><span class="Loading-bar1"></span><span class="Loading-bar2"></span><span class="Loading-bar3"></span></div></div>');
		
		jthis.append(this.container)
			 .append(btnClose)
			 .append(btnPrev)
			 .append(btnNext)
		;
		
		btnNext.click(function(){refThis.next();})
		btnPrev.click(function(){refThis.prev();})
		btnClose.click(function(){refThis.close();})
		
		var tab = [this.container, btnClose, btnNext, btnPrev];
		TweenLite.set(tab, {autoAlpha:0});
		this.tlShow = new TimelineLite({onReverseComplete:function(){refThis._closeComplete();}})
			.fromTo(jthis, 1.2, {height:0}, {height:"100%", ease:Power4.easeInOut, onComplete:function(){refThis.resize();}})
			.to(tab, .5, {autoAlpha:1})
		;
			
		return jthis;
	};
	

});