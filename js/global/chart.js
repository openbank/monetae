require([
	"Chart",
],function(Chart) {
    
        if($('#chart1').length > 0){

            /* Premier camembert */
            var data1 = [
                {
                    value: 27,
                    color: "#fff",
                    highlight: "#1a1a1a",
                    label: "Civique"
                },
                {
                    value: 36,
                    color: "#fff",
                    highlight: "#1a1a1a",
                    label: "Éducation formation"
                },
                {
                    value: 91,
                    color: "#fff",
                    highlight: "#1a1a1a",
                    label: "Culture"
                },
                {
                    value: 43,
                    color: "#fff",
                    highlight: "#1a1a1a",
                    label: "Corporate"
                },
                {
                    value: 19,
                    color: "#fff",
                    highlight: "#1a1a1a",
                    label: "Hôtellerie"
                },
                {
                    value: 6,
                    color: "#fff",
                    highlight: "#1a1a1a",
                    label: "Autres"
                }
            ];

            /* Options */
            var options = {
                segmentShowStroke : true,
                segmentStrokeColor : "#f44336",
                segmentStrokeWidth : 8,
                percentageInnerCutout : 50,
                animationSteps : 70,
                animationEasing : "easeOutQuart",
                animateRotate : true,
                animateScale : false,
                legendTemplate : "<ul class=\"<%=name.toLowerCase()%>-legend\"><% for (var i=0; i<segments.length; i++){%><li><span style=\"background-color:<%=segments[i].fillColor%>\"></span><%if(segments[i].label){%><%=segments[i].label%><%}%></li><%}%></ul>"
            };


            var ctx = document.getElementById("chart1").getContext("2d");
            var myDoughnutChart = new Chart(ctx).Doughnut(data1, options);

        }

});