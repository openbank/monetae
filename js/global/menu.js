require([],function() {
    
    
    /**
    * Menu principal
    **/
    var jBtn = $('#btnMenu'),
        jMenu = $('.Menu'),
        jParent = $('.menu-item-has-children > a'),
        _isAnim = false,
        menuWidth = "37.5%"
    ;
    
    resizeMenu();
    $( window ).resize(function() {
        resizeMenu();
    });
    
    // Ouverture / fermeture du menu
    jBtn.click(function(){
        
        if(!_isAnim){
            _isAnim = true;
            
            if(!jBtn.hasClass('isMenuOpen')){
                jBtn.addClass('isMenuOpen');
                openMenu();
            }
            else{
                jBtn.removeClass('isMenuOpen');
                closeMenu();
            }
        }
        
        return false;
    });
    
    
    jParent.click(function(){

        var elem = $(this).next('.sub-menu');
        openItem(elem);

        return false;

    });
    
    
    var jVoletIntro = $('.Volet.intro'),
        jVoletMenu = $('.Volet.menu'),
        jIntroTexte = $('.Volet-jumbo', jVoletIntro),
        jIntroFooter = $('.Volet-footer', jVoletIntro),
        jMenuContent = $('.Volet-header, .Volet-nav, .Volet-footer', jVoletMenu),
        jLogo = $('.SiteHeader-logo svg')
    ;
    
    
    
    
    /**
    * Resize menu
    **/
    function resizeMenu(){
        
        if (window.matchMedia("(max-width: 980px)").matches) {
          menuWidth = "100%";
        } else {
          menuWidth = "37.5%";
        }
        
    }
    
    
    
    /**
    * Animation d'apparition du menu
    **/
    function openMenu(){
        
        vTL = new TimelineLite({onComplete:function(){
            _isAnim = false;
        }})
          .set(jLogo, {css:{className:'+=red'}})
          .set(jMenu, {'display': 'table', autoAlpha: 1})
          .fromTo(jVoletIntro, 0.9, {left: "-63%"}, {left: "0%", ease: Power4.easeOut})
          .fromTo(jVoletMenu, 0.9, {left: menuWidth}, {left: "0%", ease: Power4.easeOut}, "=-.9")
          .fromTo(jIntroFooter, 1.8, {autoAlpha: 0, x: -60}, {autoAlpha: 1, x: 0, ease: Power4.easeOut}, "=-.5")
          // y ne fonctionne pas sur .Volet-jumbo, donc on utilise "top"
          .fromTo(jIntroTexte, 1.8, {autoAlpha: 0, left: -60}, {autoAlpha: 1, left: 0, ease: Power4.easeOut}, "=-1.8")
          .fromTo(jMenuContent, 1.8, {autoAlpha: 0, x: 60}, {autoAlpha: 1, x: 0, ease: Power4.easeOut}, "=-1.8")
        ;

        vTL.seek(vTL.totalDuration()).pause(0);
        vTL.play();
        
    }
    
    
    
    
    /**
    * Animation de disparition du menu
    **/
    function closeMenu(){
        
        vTL = new TimelineLite({onComplete:function(){
            _isAnim = false;
        }})
          .to(jMenuContent, .6, {autoAlpha: 0, x: 60, ease: Power1.easeIn})
          .to(jIntroTexte, .6, {autoAlpha: 0, left: -60, ease: Power1.easeIn}, "=-.6")
          .to(jIntroFooter, .6, {autoAlpha: 0, x: -60, ease: Power1.easeIn}, "=-.6")
          .to(jVoletMenu, .9, {left: menuWidth, ease: Power4.easeOut}, "=-.2")
          .to(jVoletIntro, .9, {left: "-63%", ease: Power4.easeOut}, "=-.9")
          .set(jLogo, {css:{className:'-=red'}}, "=-.5")
          .set(jMenu, {'display': 'none', autoAlpha: 0})
        ;

        vTL.seek(vTL.totalDuration()).pause(0);
        vTL.play();
        
    }
    
    
    
    
    /**
    * Animation d'apparition d'un sous menu
    **/
    function openItem(elem){
        
        var jLiElem = $('li', elem);

        /**
        * Si aucun élément est ouvert : on ouvre l'élément courant
        **/
        if(!elem.hasClass('isItemOpen')){

            vTL = new TimelineLite()
                .to(elem, .5, {css:{className:'+=isItemOpen'}, ease: Power4.EaseOut})
            ;
            vTL.seek(vTL.totalDuration()).pause(0);
            vTL.play();

        }

        /**
        * Si l'élément courant est déjà ouvert : on le replie
        **/
        else if(elem.hasClass('isItemOpen')){

            vTL = new TimelineLite()
                .to(elem, .5, {css:{className:'-=isItemOpen'}, ease: Power4.EaseIn})
            ;
            vTL.seek(vTL.totalDuration()).pause(0);
            vTL.play();

        }
        
    }
    

});