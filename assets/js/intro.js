var $hiddenGroup = $('section:not(#opening-section), header>.header-inner, footer, p, img, .btn:not(.nav-right>button)');
var innerText = '<b>Hello, World!</b> <br/>The programmable bank is here. Sign up now for early bird access.';

if (localStorage.getItem('viewed-intro') === null) {
  $hiddenGroup.addClass('hidden');
  $('#opening').html('');
  $('body').addClass('yellow');

  $(function() {
    setTimeout(function() {
      $('#opening').typed({
        strings: [
          innerText
        ],
        typeSpeed: 14,
        showCursor: false,
        callback: function() {
          if (isLocalStorageNameSupported()) {
            localStorage.setItem('viewed-intro', 'true');
          }

          $('#opening').addClass('cursor-hidden');

          $('body').addClass('white');

          $hiddenGroup.addClass('visible');
        }
      });
    }, 2000);
  });
} 
else {
  $('#opening').addClass('cursor-hidden');
}

function isLocalStorageNameSupported() {
  var testKey = 'test', storage = window.localStorage;
  try {
    storage.setItem(testKey, '1');
    storage.removeItem(testKey);
    return true;
  } catch (error) {
    return false;
  }
}
