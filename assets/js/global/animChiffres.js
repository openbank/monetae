require([],function() {
    
    var jBandeau = $('.Bandeau'),
        scrollTop = $(window).scrollTop() + $(window).height(),
        _isShown = false;
     
    update();
    $(window).bind('scroll', function() {
        update();
    });
    
     
    /**
    * On vérifie au scroll si le bandeau doit s'afficher ou non
    **/
    function update(){
        
        scrollTop = $(window).scrollTop() + $(window).height();
        
         jBandeau.each(function(){
             var offsetTop = $(this).offset().top;
         
             if (scrollTop - 100 > offsetTop) {
                showBandeau();
             }
         });
        
    }
    
    
    
    
    /**
    * On affiche le bandeau
    **/
    function showBandeau(){
        
        if(!_isShown){
            var jOdometer = document.querySelectorAll('.odometer');
            _isShown = true;

            /*var i = 0;
            launchCounter(jOdometer[0]);
            i++;
            var interval = setInterval(function(){
                launchCounter(jOdometer[i]);
                i++;

                if(i >= jOdometer.length){
                    clearInterval(interval);
                }
            }, 300);*/

            for(var i = 0; i < jOdometer.length; i++){
                launchCounter(jOdometer[i]);   
            }
        }
        
    }
    
    
    
    /**
    * Launch animation
    **/
    function launchCounter(elem){
        var jTexte = $(elem).next('.Bandeau-suffix');
        animateCounter(elem);
        setTimeout(function () {
            TweenLite.to(jTexte, 1.4, {css:{className:'+=shown'}, ease: Power4.easeOut});
        }, 2000);
        
    }
    
    
     
    /**
    * Animate counter
    **/
    function animateCounter(elem) {
        setTimeout(function () {
            max = elem.getAttribute("data-goto");
            od = new Odometer({
                el: elem,
                value: 333555,
                format: 'd',
                animation: 'count',
                duration: 600
            });
            od.update(max);
        }, 300);
    }
     
     
     
     
     
});