require([
    "TweenLite",
    "CSSPlugin",
    "EasePack",
    "TimelineLite"
],function(TweenLite, CSSPlugin, EasePack, TimelineLite) {
    
    
    
    var jHeader = $('.SiteHeader'),
        jPageHeader = $('.PageHeader'),
        jPageHeaderH1 = $('.PageHeader-titles h1'),
        jPageHeaderH2 = $('.PageHeader-titles h2'),
        jPageHeaderBg = $('.PageHeader-bg'),
        jPageHeaderTitles = $('.PageHeader-bigTitle'),
        jDegrade = $('.PageHeader-degrade'),
        jGoBot = $('.PageHeader-gobot'),
        jLoading = $('.Loading'),
        jLoader = $('.Loading .Loading-loader'),
        reload = true;
    ;
    
    // Debug firefox (retour affiche l'état final de la page précédente, avec le loader lancé)
    pageLoader();
    window.onunload = function(){
        pageLoader();
    }; 
    
    
    /**
    * Loader page
    **/
    function pageLoader(){

        // Preload
        if(jPageHeaderBg.length > 0){
            TweenLite.to(jLoader, 0.5, {css:{className:'+=shown'}});
            if(jPageHeaderBg.get(0).complete){
                loadComplete();
            }
            else{
                jPageHeaderBg.load(function() {
                    loadComplete();
                });
            }
        }
        else{
            loadComplete();
        }

    }
    
    
    
    /**
    * Chargement de la page terminé
    **/
    function loadComplete(){
        
        var img = $('.PageHeader-bg')[0];
        if (img && !img.width){
            return setTimeout(loadComplete, 10);
        }
        
        vTL = new TimelineLite({onComplete:function(){
            showController();
        }})
          .to(jLoading, .6, {css:{className:'-=shown'}})
          .to(jLoader, .3, {css:{className:'-=shown'}}, "=-.6")
          .set(jLoading, {css:{className:'+=hidden'}})
        ;

        vTL.seek(vTL.totalDuration()).pause(0);
        vTL.play();
    }
    
    
    
    /**
    * On choisit le type d'anim d'intro
    **/
    function showController(){
        if($('.home').length > 0){
            showHome();
        }
        else if($('.split').length > 0){
            showProjet();
        }
        else if($('.full').length > 0){
            showPage();
        }
        else{
            showDefault(); 
        }
    }
    
    

    /**
    * Animation d'apparition sur la page d'accueil
    **/
    function showHome(){
        
        vTL = new TimelineLite({onComplete:function(){
            TweenLite.to(jGoBot, 2, {css:{className:'+=shown'}})
            animVerbe();
            updateTitle('full');
        }})
            .fromTo(jPageHeader, 2, {autoAlpha:0, scale:1.1}, {autoAlpha:1, scale:1, transformOrigin:"50% 50%", ease:Power4.easeOut})
            .fromTo(jDegrade, 1.5, {autoAlpha:0}, {autoAlpha:1, transformOrigin:"50% 50%", ease: Power4.easeOut}, "=-1.5")
            .to(jHeader, 1.2, {css:{className:'+=shown'}, ease: Power4.easeOut}, "=-1.2")
            .to(jPageHeaderH1, 1.2, {css:{className:'+=shown'}, ease: Power4.easeOut}, "=-1")
            .to(jPageHeaderH2, 1.2, {css:{className:'+=shown'}, ease: Power4.easeOut}, "=-1")
        ;

        vTL.seek(vTL.totalDuration()).pause(0);
        vTL.play();
        
    }
    
    
    
    
    /**
    * Animation d'apparition sur les pages speciales
    **/
    function showProjet(){
        
        vTL = new TimelineLite({onComplete:function(){
            TweenLite.to(jGoBot, 2, {css:{className:'+=shown'}})
            updateTitle('full');
        }})
          .fromTo(jPageHeader, 2, {autoAlpha:0, scale:1.15}, {autoAlpha:1, scale:1, transformOrigin:"50% 50%", ease:Power4.easeOut})
          .fromTo(jDegrade, 1.5, {autoAlpha:0}, {autoAlpha:0.7, transformOrigin:"50% 50%", ease: Power4.easeOut}, "=-1.5")
          .to(jPageHeaderTitles, 1.2, {css:{className:'+=shown'}, ease: Power4.easeOut}, "=-.8")
          .to(jPageHeaderH2, 1.2, {css:{className:'+=shown'}, ease: Power4.easeOut}, "=-.8")
        ;

        vTL.seek(vTL.totalDuration()).pause(0);
        vTL.play();
        
    }
    
    
    
    
    /**
    * Animation d'apparition sur les pages speciales
    **/
    function showPage(){
        vTL = new TimelineLite({onComplete:function(){
            TweenLite.to(jGoBot, 2, {css:{className:'+=shown'}})
            updateTitle('normal');
        }})
            .fromTo(jPageHeader, 2, {autoAlpha:0, scale:1.15}, {autoAlpha:1, scale:1, transformOrigin:"50% 50%", ease:Power4.easeOut})
            .fromTo(jDegrade, 1.5, {autoAlpha:0}, {autoAlpha:0.7, transformOrigin:"50% 50%", ease: Power4.easeOut}, "=-1.5")
            .to(jPageHeaderH1, 1.2, {css:{className:'+=shown'}, ease: Power4.easeOut}, "=-1")
            .to(jPageHeaderH2, 1.2, {css:{className:'+=shown'}, ease: Power4.easeOut}, "=-1")
        ;

        vTL.seek(vTL.totalDuration()).pause(0);
        vTL.play();
        
    }
    
    
    
    
    /**
    * Animation d'apparition sur les pages standards
    **/
    function showDefault(){
        
        vTL = new TimelineLite()
          .to(jHeader, 1.2, {css:{className:'+=shown'}, ease: Power4.easeOut})
        ;

        vTL.seek(vTL.totalDuration()).pause(0);
        vTL.play();
        
    }

    
    
    /**
    * Apparition / disparition des titres principaux au scroll
    **/  
    function updateTitle(tag){
        titleScroll(tag);
        $(window).bind('scroll', function() {
            titleScroll(tag);
        });
    }
    
    var jWin = $(window),
        jTitles = $('.PageHeader-titles'),
        titlesHeight = jTitles.height(),
        titlesOffset = jTitles.offset().top
    ;
    
    function titleScroll(tag){
        
        // Sur les pages avec un grand visuel
        if(tag == "full"){
            if (jWin.scrollTop() > 150){

                vTL = new TimelineLite()
                  .to(jTitles, 1, {autoAlpha: 0, y: -30, ease: Power3.easeOut})
                ;

                vTL.seek(vTL.totalDuration()).pause(0);
                vTL.play();
            }
            // On fait disparaitre
            else{
                vTL = new TimelineLite()
                  .to(jTitles, 1, {autoAlpha: 1, y: 0, ease: Power3.easeOut})
                ;

                vTL.seek(vTL.totalDuration()).pause(0);
                vTL.play();
            }
        }
        
        
        // Sur les pages avec un grand visuel
        if(tag == "normal"){
            if (jWin.scrollTop() > 75){

                vTL = new TimelineLite()
                  .to(jTitles, 1, {autoAlpha: 0, y: -30, ease: Power3.easeOut})
                ;

                vTL.seek(vTL.totalDuration()).pause(0);
                vTL.play();
            }
            // On fait disparaitre
            else{
                vTL = new TimelineLite()
                  .to(jTitles, 1, {autoAlpha: 1, y: 0, ease: Power3.easeOut})
                ;

                vTL.seek(vTL.totalDuration()).pause(0);
                vTL.play();
            }
        }
    }
    

    
    
    /**
    * Animation des verbes du titre sur la page d'accueil
    **/
    var jVerbes = $('.verbe'),
        vCurrent = 0,
        nextCurrent = vCurrent + 1,
        timeout = null
    ;
    
    TweenLite.set(jVerbes, {autoAlpha:0, y:30});
    TweenLite.set(jVerbes.eq(0), {autoAlpha:1, y:0});
    
    function animVerbe(){
        
        vTL = new TimelineLite({onComplete:function(){
            timeout = setTimeout(function(){
                vCurrent = (vCurrent < jVerbes.length-1) ? vCurrent + 1 : 0;
                nextCurrent = (vCurrent < jVerbes.length-1) ? vCurrent + 1 : 0;
                animVerbe();
            }, 3000);
        }})
          .fromTo(jVerbes.eq(vCurrent), 1, {autoAlpha:1, y:0}, {autoAlpha:0, y:-30, ease: Power4.easeOut})
          .fromTo(jVerbes.eq(nextCurrent), 1, {autoAlpha:0, y:30}, {autoAlpha:1, y:0, ease: Power4.easeOut}, "=-.7")
        ;

        vTL.seek(vTL.totalDuration()).pause(0);
        vTL.play();
    }

});