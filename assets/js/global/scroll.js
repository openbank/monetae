require([
    "TweenLite",
    "CSSPlugin",
    "EasePack",
    "TimelineLite"
],function(TweenLite, CSSPlugin, EasePack, TimelineLite) {
  
    
    var jWin = $(window),
        scrollTop = jWin.scrollTop() + jWin.height(),
        jHeader = $(".HeaderBg"),
        jPageLayout = $(".PageLayout")
    ;
    
    menuScroll();
    $(window).bind('scroll', function() {
        menuScroll();
        goBotScroll();
    });
    

    /**
    * Etat du menu au scroll
    **/
    function menuScroll(){
        
        if (jWin.scrollTop() > jPageLayout.offset().top - 60){
            TweenLite.set(jHeader, {css:{className:'+=fixed'}});
        }
        else{
            TweenLite.set(jHeader, {css:{className:'-=fixed'}});
        }
    }
    
    
        /**
    * Acceder au contenu
    **/
    var jBtn = $('.PageHeader-gobot');

    // Au clic sur l'icône, on scroll
    function goBotScroll(){
        if (jWin.scrollTop() > 0){
            TweenLite.to(jBtn, 0.4, {autoAlpha:0});
        }
        else{
            TweenLite.to(jBtn, 0.4, {autoAlpha:1});
        }
    }



    
});


