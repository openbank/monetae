require([
	"Dotdotdot", "ScrollTo"
],function(Dotdotdot, ScrollTo) {
    
    
    /**
    * Ajout du roll over des blocs
    **/
    
   
    function hoverBlocs(){
        var jLinks = $('.Bloc-link').not('.Bloc-slider');
        jLinks.each(function(){
            $(this).append('<div class="bloc bloc1 horizontal"></div>' +
                            '<div class="bloc bloc2 vertical"></div>' +
                            '<div class="bloc bloc3 horizontal"></div>' +
                            '<div class="bloc bloc4 vertical"></div>'
            );
        });
    }
    
    
    dot();
    hoverBlocs();
    
    // Resize
    resize();
    $(window).resize(function(){
        resize();
    });
    
    /**
    * Au clic sur un lien interne, on fadeOut la page pour afficher le loader sur la suivante
    **/
    
    $('a').click(function(event){
        if(!event.shiftKey && !event.altKey && !event.ctrlKey) {
        
            var jLoading = $('.Loading'),
                jLoader = ('.Loading-loader')
                jBody = $('.Bloc-body'),
                jProjets = $('.projetsSecondaires'),
                myUrl = $(this).attr('href'),
                jLogo = $('.SiteHeader-logo svg'),
                site = 'http://comment.hl2.siteinternet.com'
                site2 = 'http://www.comment.fr'
            ;

            var result = myUrl.indexOf(site);
            var result2 = myUrl.indexOf(site2);
            
            
            /* -- Loader page métiers/clients -- */
            // Clic sous menu page métiers/clients

            var parent = $(this).parents('.Sidebar-menu');
            if(parent.length > 0){
                $('.Sidebar-menu li').removeClass('current_page_item');
                $(this).parent().addClass('current_page_item');
                
                TweenLite.to(jBody, .5, {css:{className:'+=notShown'}, onComplete: function(){   
                    $.get(myUrl, {}, function(data) {
                        var response = $('<div />').html(data);
                        jBody.html(response.find('.dataLoad-body'));
                        jProjets.html(response.find('.dataLoad-projets'));
                        dot();
                        hoverBlocs();
                        TweenLite.to(jBody, .5, {css:{className:'-=notShown'}});
                    },'html');
                }});
                
                return false;
            }

            
            /* -- Loader page standard -- */

            if((result != -1 || result2 != -1) && !$(this).is('.Menu .menu-item-has-children > a')){
                console.log('projet');
                vTL = new TimelineLite({onComplete:function(){
                    window.location.href = myUrl;
                }})
                  .set(jLoader, {css:{className:'-=shown'}})
                  .set(jLogo, {css:{className:'-=red'}})
                  .set(jLoading, {css:{className:'-=hidden'}})
                  .to(jLoading, .4, {css:{className:'+=shown'}})
                ;

                vTL.play();
                
                return false;
            }
            
        }
        
    });
    
    
    
    
    
    /**
    * Acceder au contenu
    **/
    var jBtn = $('.PageHeader-gobot');
    
    jBtn.click(function(){
        scrollto($(this));
        return false;  
    });

    // Au clic sur l'icône, on scroll
    function scrollto(elem){
        var id = elem.attr("href");
        var offset = $(id).offset().top;
        TweenLite.to(window, 1, {scrollTo:{y:offset}, ease:Power3.easeInOut});
        TweenLite.to(jBtn, 0.4, {autoAlpha:0});
    }
    
    
    
    
    /**
    * DotDotDot (script jQuery pour limiter les titres à deux lignes)
    **/
    function dot(){
        $(".Bloc-title, .Slider-title").dotdotdot({
            ellipsis	: '...',
            wrap		: 'letter',
            fallbackToLetter: true,
            watch		: true,
            tolerance	: 0,
            callback	: function( isTruncated, orgContent ) {},
            lastCharacter	: {
                remove		: [ ' ', ',', ';', '.', '!', '?' ],
                noEllipsis	: []
            }
        });
    }
    
    
    function resize(){
        var nua = navigator.userAgent;
        var is_android = ((nua.indexOf('Mozilla/5.0') > -1 && nua.indexOf('Android ') > -1 &&     nua.indexOf('AppleWebKit') > -1) && !(nua.indexOf('Chrome') > -1));

        if(is_android){
           $('.PageHeader, .Menu').height($(window).height());
           $('.PageLayout').css({'margin-top':$(window).height()});
        }
    }
    

});