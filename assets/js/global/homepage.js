require([],function() {
    
    var x,
        jSlider = $(".Slider-zone"),
        nbItems = $(".Slider-slide", jSlider).length,
        slideWidth = $(".Slider-slide").innerWidth(),
		currentItem = 0
	;
	
	function animate(){
		x = currentItem * slideWidth;
        TweenLite.to(jSlider, 0.8, {left: -x, ease:Power3.easeInOut});
	}
	
	$(".Slider-next").click(function(){
		currentItem = ++currentItem % nbItems;
		animate();
        return false;
	});
	$(".Slider-prev").click(function(){
		currentItem = (currentItem == 0 ? nbItems : currentItem ) - 1;
		animate();
        return false;
	});
    
});
