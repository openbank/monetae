/*===========================================================
    LISTE PROJETS
===========================================================*/

// var gps = tableau de données latitude / longitude pour le projet




/*===========================================================
    CUSTOM STYLES
===========================================================*/


function initialize() {
    // Create an array of styles.
    var styles = [
        {
            "featureType": "administrative",
            "elementType": "all",
            "stylers": [
                {
                    "visibility": "off"
                }
            ]
        },
        {
            "featureType": "administrative.country",
            "elementType": "geometry.stroke",
            "stylers": [
                {
                    "visibility": "on"
                },
                {
                    "color": "#F7F5F2"
                },
                {
                    "weight": 0.8
                }
            ]
        },
        {
            "featureType": "water",
            "elementType": "all",
            "stylers": [
                {
                    "color": "#F7F5F2"
                }
            ]
        },
        {
            "featureType": "landscape",
            "elementType": "all",
            "stylers": [
                {
                    "color": "#DDD4CB"
                }
            ]
        },
        {
            "featureType": "poi",
            "elementType": "all",
            "stylers": [
                {
                    "color": "#DDD4CB"
                }
            ]
        },
        {
            "featureType": "road",
            "elementType": "all",
            "stylers": [
                {
                    "visibility": "off"
                }
            ]
        },
        {
            "featureType": "transit",
            "elementType": "all",
            "stylers": [
                {
                    "visibility": "off"
                }
            ]
        },
        {
            "featureType": "all",
            "elementType": "labels",
            "stylers": [
                {
                    "visibility": "off"
                }
            ]
        }
    ];

    var styledMap = new google.maps.StyledMapType(styles, {name: "Styled Map"});

    
    
    
/*===========================================================
    CREATION DE L'OBJET MAP
===========================================================*/

    var mapOptions = {
            center: new google.maps.LatLng(gps.latitude, gps.longitude),
            zoom: 4,
            minZoom: 3,
            disableDefaultUI: true,
            zoomControl: false,
            scrollwheel: false,
            draggable: false,
            disableDoubleClickZoom: true,
            zoomControlOptions: {
            style: google.maps.ZoomControlStyle.SMALL
        },
        mapTypeId: google.maps.MapTypeId.ROADMAP
    };

    var map = new google.maps.Map(document.getElementById("map-canvas"),
    mapOptions);

    // Ajout du style perso
    map.mapTypes.set('map_style', styledMap);
    map.setMapTypeId('map_style');
	
	// Markers
    setMarker(map, gps);

} 





/*===========================================================
    MARQUEURS GMAP
===========================================================*/

function setMarker(map, location) {

	// Style du marqueur
	var marker = {
	  path: google.maps.SymbolPath.CIRCLE,
	  scale: 7,
	  strokeColor: '#f44336',
	  strokeWeight: 2,
	  fillColor: "#ddd4cb",
	  fillOpacity: 1
	};
    
    
    // Marqueur
    var myLatLng = new google.maps.LatLng(location.latitude, location.longitude);
    var marker = new google.maps.Marker({
        position: myLatLng,
        map: map,
        icon: marker,
        clickable: false
    });

}



google.maps.event.addDomListener(window, 'load', initialize);
